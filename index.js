module.exports = {
    get Issue () {
        return require('./Issue');
    },

    get Jira () {
        return require('./Jira');
    }
};
